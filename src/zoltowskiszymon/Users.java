package zoltowskiszymon;

public class Users {
	
	private int personID;
	private String login;
	private String password;
	
	public int getPersonID(){
		return personID;
	}
	
	public void setPersonId(int personID){
		this.personID = personID;
	}
	
	public String getLogin(){
		return login;
	}
	
	public void setLogin(String login){
		this.login = login;
	}
	
	public String getPassword(){
		return password;
	}
	
	public void setPassword(String password){
		this.password = password;
	}
	
}
